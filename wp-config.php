<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'madrela_wpress');

/** MySQL database username */
define('DB_USER', 'madrela');

/** MySQL database password */
define('DB_PASSWORD', 'e6a6atyme');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

// Comment for production
// define('WP_HOME','http://localhost/madrela/root/');
// define('WP_SITEURL','http://localhost/madrela/root/');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+pO11OZ{<Pc#[&Za:yk?u>1pG?jd3+t$/WD/R3d1LR2TU$|y`BKOD9v#7%,r#K|Q');
define('SECURE_AUTH_KEY',  '3g@IjWk/13SIlzDNAF3I q}teU,}|E4Oj]]d16mx_*G4_V0=B0^@Q:r7*whCf(-7');
define('LOGGED_IN_KEY',    'r_3$EhmvzZp-Z]k8vZX+RUMQ+j[P<;Dx6D;]5~E`I}]ZshkGYmbWl?3EA>+rzhM#');
define('NONCE_KEY',        'RwZ&Qw9aNlo{*8fbvp5.VYmr@ paPQa2d`eBk3X.M*/+@Yal5aC%^3?i[(,|b0I7');
define('AUTH_SALT',        '6>rQBANJMP#s) aLzi{wtTD}]szh#N+7y=0`C2>skap4|x&(HNsOaV _vd2^`3KY');
define('SECURE_AUTH_SALT', 'B=.yy!TeZ5 ;j+S&Ia^bQNaOF3]Wb?+9Ga%8-XFRd4oMh]WM7HyN7p7{kRZ%n_&t');
define('LOGGED_IN_SALT',   '=$1w<X;$+T:PyFcO,jolP>]w5*6Q-@6.AQc%|{PD-i+O,U/>|346RXTX$5:@Se2P');
define('NONCE_SALT',       '|DKoi[#Wz4z4QDInVkw!H`bn,,viQ!5*xr@cr-`~aL~h-2Pz`O|pYqTv8-wqEmA[');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
