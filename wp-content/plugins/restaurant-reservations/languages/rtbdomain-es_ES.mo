��    h      \      �      �     �     �     �     �     �     �     �     �     �               $  *   1  0   \  0   �     �  n   �  h   6  r  �     
     
  M   &
     t
  	   �
  S   �
     �
     �
     �
  �    �  �  Q   �  �   �     m     t     �     �  	   �  <   �      �  #   �  "   "     E     \  J   a  N   �  2   �  6   .  4   e  6   �     �     �     �     �               "     =     C     K  %   Q  =   w  -   �  -   �  9        K     _     o     �     �  ^   �               *     @  ,   I  =   v  :   �  ;   �  ;   +  3   g  3   �  +   �     �  0        3     C  p   Z  W   �  P   #     t     �     �  0   �  *   �     �     �                !     .  7   D  7   |     �  �  �     s     �     �     �     �     �     �     �     �               5     G  
   O  	   Z     d     m     v  $   |     �     �     �     �  
   �     �                 �   .  u  �     V!     q!     �!     �!     �!     �!     �!  ,   �!     �!     �!     "     "     "     "     "     ,"     0"     7"     <"     H"     U"     ]"     `"     g"     v"  &   �"     �"  	   �"     �"  .   �"  J   #  -   X#  $   �#  >   �#     �#      $     $     &$     )$  =   9$     w$  	   ~$     �$     �$  2   �$  J   �$  M   '%  N   u%  P   �%  :   &  ;   P&  -   �&     �&     �&     �&     �&  ]   �&  I   ='  J   �'     �'     �'     �'     �'     �'     �'     �'      (     (     (     (     5(  4   G(     |(   Add New Add New Booking Add a Message Add new scheduling rule All All Bookings Apply Book a table Booking Booking Form Booking Manager Booking Page Booking status for a closed bookingClosed Booking status for a confirmed bookingConfirmed Booking status when it is pending reviewPending Bookings Brief default description of a scheduling rule when all the weekdays/weeks are included in the rule.Every day Brief default description of a scheduling rule when no weekdays or weeks are included in the rule.Never Brief default description of a scheduling rule when some weekdays are included on only some weeks of the month. The {days} and {weeks} bits should be left alone and will be replaced by a comma-separated list of days (the first one) and weeks (the second one) in the following format: M, T, W on the first, second week of the month{days} on the {weeks} week of the month Clear Filter Closed Closed <span class="count">(%s)</span> Closed <span class="count">(%s)</span> Confirm this booking Confirmed Confirmed <span class="count">(%s)</span> Confirmed <span class="count">(%s)</span> Contact Details Date Date Format Default email sent to the admin when a new booking request is made. The tags in {brackets} will be replaced by the appropriate content and should be left in place. HTML is allowed, but be aware that many email clients do not handle HTML very well.A new booking request has been made at {site_name}:

{user_name}
{party} people
{date}

{bookings_link}
{confirm_link}
{close_link}

&nbsp;

<em>This message was sent by {site_link} on {current_time}.</em> Default email sent to users when they make a new booking request. The tags in {brackets} will be replaced by the appropriate content and should be left in place. HTML is allowed, but be aware that many email clients do not handle HTML very well.Thanks {user_name},

Your booking request is <strong>waiting to be confirmed</strong>.

Give us a few moments to make sure that we've got space for you. You will receive another email from us soon. If this request was made outside of our normal working hours, we may not be able to confirm it until we're open again.

<strong>Your request details:</strong>
{user_name}
{party} people
{date}

&nbsp;

<em>This message was sent by {site_link} on {current_time}.</em> Default email subject for admin notifications of new bookingsNew Booking Request Default email subject sent to user when they request a booking. %s will be replaced by the website nameYour booking at %s is pending Delete Delete rule Edit Booking End Date End Date: Enter the message to display when a booking request is made. Format of a scheduling ruleDate Format of a scheduling ruleMonthly Format of a scheduling ruleWeekly Friday abbreviationFr Help Label for selecting days of the week in a scheduling ruleDays of the week Label for selecting weeks of the month in a scheduling ruleWeeks of the month Label for the ending time of a scheduling ruleEnd Label for the starting time of a scheduling ruleStart Label to select time slot for a scheduling ruleTime Label to set a scheduling rule to last all dayAll day Max Party Size Message Monday abbreviationMo Name New Booking No bookings found No bookings found in trash Party Pending Phone Please enter a name for this booking. Please enter an email address so we can confirm your booking. Please enter the date you would like to book. Please enter the time you would like to book. Please let us know how many people will be in your party. Reject this booking Request Booking Restaurant Bookings Saturday abbreviationSa Search Bookings Select a page on your site to automatically display the booking form and confirmation message. Set To Closed Set To Confirmed Set To Pending Review Settings Sorry, bookings can not be made in the past. Sorry, bookings can not be made more than %s days in advance. Sorry, bookings must be made more than %s days in advance. Sorry, bookings must be made more than %s hours in advance. Sorry, bookings must be made more than %s mings in advance. Sorry, no bookings are being accepted at that time. Sorry, no bookings are being accepted on that date. Sorry, no bookings are being accepted then. Status Status label for bookings put in the trashTrash Success Message Sunday abbreviationSu Thanks, your booking request is waiting to be confirmed. Updates will be sent to the email address you provided. The date you entered is not valid. Please select from one of the dates in the calendar. The time you entered is not valid. Please select from one of the times provided. Thursday abbreviationTh Time Title Title of admin page that lists bookingsBookings Title of bookings admin menu itemBookings Today Trash Tuesday abbreviationTu Upcoming View Booking View pending bookings View the help documentation for Restaurant Reservations We only accept bookings for parties of up to %d people. Wednesday abbreviationWe Project-Id-Version: Restaurant Reservations
Report-Msgid-Bugs-To: 
POT-Creation-Date: Thu Jul 17 2014 15:43:57 GMT+0100 (GMT Daylight Time)
PO-Revision-Date: Sun Sep 14 2014 02:00:01 GMT-0500 (COT)
Last-Translator: Artyficial Media <artyficialmedia@gmail.com>
Language-Team: 
Language: Spanish
Plural-Forms: nplurals=2; plural=n != 1
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: ..
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Generator: Loco - https://localise.biz/
X-Loco-Target-Locale: es_ES Agregar nueva Agregar nueva reserva Agregar un mensaje Agregar nueva regla de reserva todo Todas las 
Reservas Aplicar Reservar una mesa Reserva Formulario de Reserva
 Administrador de Reservas Pagina de Reserva Cerrado Confirmado Pendiente Reservas Cada dia Nunca {days} en la {weeks} semana del mes. Limpiar filtro Cerrado Cerrado Cerrados Confirmar esta reserva Confirmada Confirmado Confirmadas Detalles de contacto Fecha Formato de fecha Una nueva reserva {site_name}:

{user_name}
{party} personas
{date}

{bookings_link}
{confirm_link}
{close_link}

&nbsp;

<em>Mensaje enviado {site_link} el {current_time}.</em> Gracias {user_name},

Su reserva <strong>espera confirmacion</strong>.

Permitanos procesar su reserva. 
Recibirá
 un mensaje pronto. Si este proceso es fuera de nuestras horas laborales, sera respondido en la 
próxima
 jornada.

<strong>Detalles de su reserva:</strong>
{user_name}
{party} personas
{date}

&nbsp;

<em>Mensaje enviado {site_link} el {current_time}.</em> Nueva solicitud de reserva Su reserva %s esta pendiente Borrar Eliminar regla Editar Reserva Fecha final Fecha final: El mensaje cuando una reserva esta realizada Fecha Mensual Semanal Vi Ayuda Dias Semana del mes Fin Inicio Hora Todo el dia Grupo maximo Mensaje Lu Nombre Nueva 
Reserva No se encontraron reservas No se encontraron reservas en papelera Numero de Personas Pendiente Telefono Por favor ingrese un nombre para esta reserva. Por favor ingrese una dirección de email para poder confirmar su reserva. Por favor ingrese la fecha que desea reservar Por favor la hora que desea reservar Por favor dejenos saber para cuantos asistentes es su reserva. Rechazar esta reserva Hacer Reserva Reservas de Restaurante Sa Buscar 
Reserva Seleccione una pagina para mostrar el formulario de reservas. Cerrar Confirmar Pendiente Revision Configuracion Las reservas no se pueden hacer en fechas pasadas. Disculpe. Las reservas no pueden ser con mas 
 %s dias de 
anticipación
. Disculpe, las reservas deben ser hechas con mas de %s dias de anticipación.  Disculpe, las reservas deben ser hechas con mas de %s horas de anticipación.  Disculpe, las reservas deben ser hechas con mas de %s minutos de anticipación.  Disculpe, no se  están realizando reservas para esa hora. Disculpe, no se  están realizando reservas para esa fecha. Disculpe, no se 
están
 realizando reservas. Estatus Papelera Mensaje de exito Do Gracias, su reserva esta esperando confirmación. Las novedades le serán enviadas por email. La fecha ingresada no es valida, por favor seleccione una del calendario. La hora ingresada no es valida. 
Por favor seleccione una de las opciones. Ju Hora Titulo Reservas Reservas Hoy Papelera Ma Proximamente Ver 
Reserva Ver reservas pendientes Ver documentacion Solo aceptamos reservas para grupos de 
%d personas. Mi 