msgid ""
msgstr ""
"Project-Id-Version: Restaurant Reservations\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: Thu Jul 17 2014 15:43:57 GMT+0100 (GMT Daylight Time)\n"
"PO-Revision-Date: Sun Sep 14 2014 02:00:01 GMT-0500 (COT)\n"
"Last-Translator: Artyficial Media <artyficialmedia@gmail.com>\n"
"Language-Team: \n"
"Language: Spanish\n"
"Plural-Forms: nplurals=2; plural=n != 1\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: ..\n"
"X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;"
"__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;"
"_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;"
"esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;"
"esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2\n"
"X-Generator: Loco - https://localise.biz/\n"
"X-Loco-Target-Locale: es_ES"

#: ../includes/Settings.class.php:84
msgctxt ""
"Default date format for display. Must match formatting rules at http://amsul."
"ca/pickadate.js/date.htm#formatting-rules"
msgid "mmmm d, yyyy"
msgstr ""

#: ../includes/Settings.class.php:85
msgctxt ""
"Default time format for display. Must match formatting rules at http://amsul."
"ca/pickadate.js/time.htm#formats"
msgid "h:i A"
msgstr ""

#: ../includes/Settings.class.php:86
msgctxt "Default interval in minutes when selecting a time."
msgid "30"
msgstr ""

#: ../includes/Settings.class.php:135
#, php-format
msgctxt ""
"Default email subject sent to user when their booking is confirmed. %s will "
"be replaced by the website name"
msgid "Your booking at %s is confirmed"
msgstr ""

#: ../includes/Settings.class.php:136
msgctxt ""
"Default email sent to users when they make a new booking request. The tags "
"in {brackets} will be replaced by the appropriate content and should be left "
"in place. HTML is allowed, but be aware that many email clients do not "
"handle HTML very well."
msgid ""
"Hi {user_name},\n"
"\n"
"Your booking request has been <strong>confirmed</strong>. We look forward to "
"seeing you soon.\n"
"\n"
"<strong>Your booking:</strong>\n"
"{user_name}\n"
"{party} people\n"
"{date}\n"
"\n"
"&nbsp;\n"
"\n"
"<em>This message was sent by {site_link} on {current_time}.</em>"
msgstr ""

#: ../includes/Settings.class.php:153
#, php-format
msgctxt ""
"Default email subject sent to user when their booking is rejected. %s will "
"be replaced by the website name"
msgid "Your booking at %s was not accepted"
msgstr ""

#: ../includes/Settings.class.php:154
msgctxt ""
"Default email sent to users when they make a new booking request. The tags "
"in {brackets} will be replaced by the appropriate content and should be left "
"in place. HTML is allowed, but be aware that many email clients do not "
"handle HTML very well."
msgid ""
"Hi {user_name},\n"
"\n"
"Sorry, we could not accomodate your booking request. We're full or not open "
"at the time you requested:\n"
"\n"
"{user_name}\n"
"{party} people\n"
"{date}\n"
"\n"
"&nbsp;\n"
"\n"
"<em>This message was sent by {site_link} on {current_time}.</em>"
msgstr ""

#: ../includes/Settings.class.php:218 ../includes/Settings.class.php:219
msgid "Settings"
msgstr "Configuracion"

#: ../includes/Settings.class.php:231
msgid "General"
msgstr ""

#: ../includes/Settings.class.php:242
msgid "Booking Page"
msgstr "Pagina de Reserva"

#: ../includes/Settings.class.php:243
msgid ""
"Select a page on your site to automatically display the booking form and "
"confirmation message."
msgstr "Seleccione una pagina para mostrar el formulario de reservas."

#: ../includes/Settings.class.php:259
msgid "Max Party Size"
msgstr "Grupo maximo"

#: ../includes/Settings.class.php:260
msgid ""
"Set a maximum allowed party size for bookings. Leave it empty to allow "
"parties of any size."
msgstr ""

#: ../includes/Settings.class.php:261
msgid "No limit"
msgstr ""

#: ../includes/Settings.class.php:271
msgid "Success Message"
msgstr "Mensaje de exito"

#: ../includes/Settings.class.php:272
msgid "Enter the message to display when a booking request is made."
msgstr "El mensaje cuando una reserva esta realizada"

#: ../includes/Settings.class.php:283
msgid "Date Format"
msgstr "Formato de fecha"

#: ../includes/Settings.class.php:284
msgid ""
"Define how the date should appear after it has been selected. <a href=\"http:"
"//amsul.ca/pickadate.js/date.htm#formatting-rules\">Formatting rules</a>"
msgstr ""

#: ../includes/Settings.class.php:295
msgid "Time Format"
msgstr ""

#: ../includes/Settings.class.php:296
msgid ""
"Define how the time should appear after it has been selected. <a href=\"http:"
"//amsul.ca/pickadate.js/time.htm#formatting-rules\">Formatting rules</a>"
msgstr ""

#: ../includes/Settings.class.php:309
msgid "Language"
msgstr ""

#: ../includes/Settings.class.php:310
msgid ""
"Select a language to use for the booking form datepicker if it is different "
"than your WordPress language setting."
msgstr ""

#: ../includes/Settings.class.php:320
msgid "Booking Schedule"
msgstr ""

#: ../includes/Settings.class.php:331
msgid "Schedule"
msgstr ""

#: ../includes/Settings.class.php:332
msgid "Define the weekly schedule during which you accept bookings."
msgstr ""

#: ../includes/Settings.class.php:334
msgctxt "Monday abbreviation"
msgid "Mo"
msgstr "Lu"

#: ../includes/Settings.class.php:335
msgctxt "Tuesday abbreviation"
msgid "Tu"
msgstr "Ma"

#: ../includes/Settings.class.php:336
msgctxt "Wednesday abbreviation"
msgid "We"
msgstr "Mi"

#: ../includes/Settings.class.php:337
msgctxt "Thursday abbreviation"
msgid "Th"
msgstr "Ju"

#: ../includes/Settings.class.php:338
msgctxt "Friday abbreviation"
msgid "Fr"
msgstr "Vi"

#: ../includes/Settings.class.php:339
msgctxt "Saturday abbreviation"
msgid "Sa"
msgstr "Sa"

#: ../includes/Settings.class.php:340
msgctxt "Sunday abbreviation"
msgid "Su"
msgstr "Do"

#: ../includes/Settings.class.php:355
msgid "Exceptions"
msgstr ""

#: ../includes/Settings.class.php:356
msgid ""
"Define special opening hours for holidays, events or other needs. Leave the "
"time empty if you're closed all day."
msgstr ""

#: ../includes/Settings.class.php:362
msgctxt "Brief description of a scheduling exception when no times are set"
msgid "Closed all day"
msgstr ""

#: ../includes/Settings.class.php:373
msgid "Early Bookings"
msgstr ""

#: ../includes/Settings.class.php:374
msgid "Select how early customers can make their booking."
msgstr ""

#: ../includes/Settings.class.php:377
msgid "Any time"
msgstr ""

#: ../includes/Settings.class.php:378
msgid "Up to 1 day in advance"
msgstr ""

#: ../includes/Settings.class.php:379
msgid "Up to 1 week in advance"
msgstr ""

#: ../includes/Settings.class.php:380
msgid "Up to 2 weeks in advance"
msgstr ""

#: ../includes/Settings.class.php:381
msgid "Up to 30 days in advance"
msgstr ""

#: ../includes/Settings.class.php:382
msgid "Up to 90 days in advance"
msgstr ""

#: ../includes/Settings.class.php:393
msgid "Late Bookings"
msgstr ""

#: ../includes/Settings.class.php:394
msgid "Select how late customers can make their booking."
msgstr ""

#: ../includes/Settings.class.php:397
msgid "Up to the last minute"
msgstr ""

#: ../includes/Settings.class.php:398
msgid "At least 15 minutes in advance"
msgstr ""

#: ../includes/Settings.class.php:399
msgid "At least 30 minutes in advance"
msgstr ""

#: ../includes/Settings.class.php:400
msgid "At least 45 minutes in advance"
msgstr ""

#: ../includes/Settings.class.php:401
msgid "At least 1 hour in advance"
msgstr ""

#: ../includes/Settings.class.php:402
msgid "At least 4 hours in advance"
msgstr ""

#: ../includes/Settings.class.php:403
msgid "At least 1 day in advance"
msgstr ""

#: ../includes/Settings.class.php:414
msgid "Date Pre-selection"
msgstr ""

#: ../includes/Settings.class.php:415
msgid ""
"When the booking form is loaded, should it automatically attempt to select a "
"valid date?"
msgstr ""

#: ../includes/Settings.class.php:418
msgid "Select today or soonest valid date"
msgstr ""

#: ../includes/Settings.class.php:419
msgid "Leave empty"
msgstr ""

#: ../includes/Settings.class.php:430
msgid "Time Interval"
msgstr ""

#: ../includes/Settings.class.php:431
msgid "Select the number of minutes between each available time."
msgstr ""

#: ../includes/Settings.class.php:434
msgid "Every 30 minutes"
msgstr ""

#: ../includes/Settings.class.php:435
msgid "Every 15 minutes"
msgstr ""

#: ../includes/Settings.class.php:436
msgid "Every 10 minutes"
msgstr ""

#: ../includes/Settings.class.php:437
msgid "Every 5 minutes"
msgstr ""

#: ../includes/Settings.class.php:446
msgid "Notifications"
msgstr ""

#: ../includes/Settings.class.php:457
msgid "Reply-To Name"
msgstr ""

#: ../includes/Settings.class.php:458
msgid "The name which should appear in the Reply-To field of a notification email"
msgstr ""

#: ../includes/Settings.class.php:469
msgid "Reply-To Email Address"
msgstr ""

#: ../includes/Settings.class.php:470
msgid ""
"The email address which should appear in the Reply-To field of a "
"notification email."
msgstr ""

#: ../includes/Settings.class.php:481
msgid "Admin Notification"
msgstr ""

#: ../includes/Settings.class.php:482
msgid ""
"Send an email notification to an administrator when a new booking is "
"requested."
msgstr ""

#: ../includes/Settings.class.php:492
msgid "Admin Email Address"
msgstr ""

#: ../includes/Settings.class.php:493
msgid "The email address where admin notifications should be sent."
msgstr ""

#: ../includes/Settings.class.php:502
msgid "Email Templates"
msgstr ""

#: ../includes/Settings.class.php:516
msgid "Template Tags"
msgstr ""

#: ../includes/Settings.class.php:518
msgid ""
"Use the following tags to automatically add booking information to the "
"emails."
msgstr ""

#: ../includes/Settings.class.php:520
msgid "Name of the user who made the booking"
msgstr ""

#: ../includes/Settings.class.php:523
msgid "Number of people booked"
msgstr ""

#: ../includes/Settings.class.php:526
msgid "Date and time of the booking"
msgstr ""

#: ../includes/Settings.class.php:529
msgid "Phone number if supplied with the request"
msgstr ""

#: ../includes/Settings.class.php:532
msgid "Message added to the request"
msgstr ""

#: ../includes/Settings.class.php:535
msgid "A link to the admin panel showing pending bookings"
msgstr ""

#: ../includes/Settings.class.php:538
msgid "A link to confirm this booking. Only include this in admin notifications"
msgstr ""

#: ../includes/Settings.class.php:541
msgid "A link to reject this booking. Only include this in admin notifications"
msgstr ""

#: ../includes/Settings.class.php:544
msgid "The name of this website"
msgstr ""

#: ../includes/Settings.class.php:547
msgid "A link to this website"
msgstr ""

#: ../includes/Settings.class.php:550
msgid "Current date and time"
msgstr ""

#: ../includes/Settings.class.php:561
msgid "Admin Notification Subject"
msgstr ""

#: ../includes/Settings.class.php:562
msgid "The email subject for admin notifications."
msgstr ""

#: ../includes/Settings.class.php:573
msgid "Admin Notification Email"
msgstr ""

#: ../includes/Settings.class.php:574
msgid ""
"Enter the email an admin should receive when an initial booking request is "
"made."
msgstr ""

#: ../includes/Settings.class.php:585
msgid "New Request Email Subject"
msgstr ""

#: ../includes/Settings.class.php:586
msgid ""
"The email subject a user should receive when they make an initial booking "
"request."
msgstr ""

#: ../includes/Settings.class.php:597
msgid "New Request Email"
msgstr ""

#: ../includes/Settings.class.php:598
msgid ""
"Enter the email a user should receive when they make an initial booking "
"request."
msgstr ""

#: ../includes/Settings.class.php:609
msgid "Confirmed Email Subject"
msgstr ""

#: ../includes/Settings.class.php:610
msgid ""
"The email subject a user should receive when their booking has been "
"confirmed."
msgstr ""

#: ../includes/Settings.class.php:621
msgid "Confirmed Email"
msgstr ""

#: ../includes/Settings.class.php:622
msgid "Enter the email a user should receive when their booking has been confirmed."
msgstr ""

#: ../includes/Settings.class.php:633
msgid "Rejected Email Subject"
msgstr ""

#: ../includes/Settings.class.php:634
msgid "The email subject a user should receive when their booking has been rejected."
msgstr ""

#: ../includes/Settings.class.php:645
msgid "Rejected Email"
msgstr ""

#: ../includes/Settings.class.php:646
msgid "Enter the email a user should receive when their booking has been rejected."
msgstr ""

#: ../includes/template-functions.php:84
msgid "Book a table"
msgstr "Reservar una mesa"

#: ../includes/template-functions.php:89 ../includes/WP_List_Table.BookingsTable.
#: class.php:267 ../lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.
#: class.php:365
msgid "Date"
msgstr "Fecha"

#: ../includes/template-functions.php:96
msgid "Time"
msgstr "Hora"

#: ../includes/template-functions.php:103 ../includes/WP_List_Table.BookingsTable.
#: class.php:268
msgid "Party"
msgstr "Numero de Personas"

#: ../includes/template-functions.php:110
msgid "Contact Details"
msgstr "Detalles de contacto"

#: ../includes/template-functions.php:115 ../includes/WP_List_Table.BookingsTable.
#: class.php:269
msgid "Name"
msgstr "Nombre"

#: ../includes/template-functions.php:122 ../includes/WP_List_Table.BookingsTable.
#: class.php:270
msgid "Email"
msgstr ""

#: ../includes/template-functions.php:129 ../includes/WP_List_Table.BookingsTable.
#: class.php:271
msgid "Phone"
msgstr "Telefono"

#: ../includes/template-functions.php:135
msgid "Add a Message"
msgstr "Agregar un mensaje"

#: ../includes/template-functions.php:141 ../includes/WP_List_Table.BookingsTable.
#: class.php:272
msgid "Message"
msgstr "Mensaje"

#: ../includes/template-functions.php:146
msgid "Request Booking"
msgstr "Hacer Reserva"

#: ../includes/WP_List_Table.BookingsTable.class.php:172 ..
#: includes/WP_List_Table.BookingsTable.class.php:235
msgid "All"
msgstr "todo"

#: ../includes/WP_List_Table.BookingsTable.class.php:173
msgid "Today"
msgstr "Hoy"

#: ../includes/WP_List_Table.BookingsTable.class.php:174
msgid "Upcoming"
msgstr "Proximamente"

#: ../includes/WP_List_Table.BookingsTable.class.php:180
msgctxt "Separator between two dates in a date range"
msgid "&mdash;"
msgstr ""

#: ../includes/WP_List_Table.BookingsTable.class.php:192
msgid "Start Date:"
msgstr ""

#: ../includes/WP_List_Table.BookingsTable.class.php:193
msgid "Start Date"
msgstr ""

#: ../includes/WP_List_Table.BookingsTable.class.php:194
msgid "End Date:"
msgstr "Fecha final:"

#: ../includes/WP_List_Table.BookingsTable.class.php:195
msgid "End Date"
msgstr "Fecha final"

#: ../includes/WP_List_Table.BookingsTable.class.php:196
msgid "Apply"
msgstr "Aplicar"

#: ../includes/WP_List_Table.BookingsTable.class.php:198
msgid "Clear Filter"
msgstr "Limpiar filtro"

#: ../includes/WP_List_Table.BookingsTable.class.php:236
msgid "Pending"
msgstr "Pendiente"

#: ../includes/WP_List_Table.BookingsTable.class.php:237
msgid "Confirmed"
msgstr "Confirmada"

#: ../includes/WP_List_Table.BookingsTable.class.php:238
msgid "Closed"
msgstr "Cerrado"

#: ../includes/WP_List_Table.BookingsTable.class.php:239
msgid "Trash"
msgstr "Papelera"

#: ../includes/WP_List_Table.BookingsTable.class.php:273
msgid "Status"
msgstr "Estatus"

#: ../includes/WP_List_Table.BookingsTable.class.php:324
msgctxt "Status label for bookings put in the trash"
msgid "Trash"
msgstr "Papelera"

#: ../includes/WP_List_Table.BookingsTable.class.php:356
msgid "Delete"
msgstr "Borrar"

#: ../includes/WP_List_Table.BookingsTable.class.php:357
msgid "Set To Confirmed"
msgstr "Confirmar"

#: ../includes/WP_List_Table.BookingsTable.class.php:358
msgid "Set To Pending Review"
msgstr "Pendiente Revision"

#: ../includes/WP_List_Table.BookingsTable.class.php:359
msgid "Set To Closed"
msgstr "Cerrar"

#: ../includes/WP_List_Table.BookingsTable.class.php:471
#, php-format
msgid "%d booking deleted successfully."
msgid_plural "%d bookings deleted successfully."
msgstr[0] ""
msgstr[1] ""

#: ../includes/WP_List_Table.BookingsTable.class.php:474
#, php-format
msgid "%d booking confirmed."
msgid_plural "%d bookings confirmed."
msgstr[0] ""
msgstr[1] ""

#: ../includes/WP_List_Table.BookingsTable.class.php:477
#, php-format
msgid "%d booking set to pending."
msgid_plural "%d bookings set to pending."
msgstr[0] ""
msgstr[1] ""

#: ../includes/WP_List_Table.BookingsTable.class.php:480
#, php-format
msgid "%d booking closed."
msgid_plural "%d bookings closed."
msgstr[0] ""
msgstr[1] ""

#: ../includes/WP_List_Table.BookingsTable.class.php:492
#, php-format
msgid "%d booking had errors and could not be processed."
msgid_plural "%d bookings had errors and could not be processed."
msgstr[0] ""
msgstr[1] ""

#: ../includes/WP_Widget.BookingFormWidget.class.php:25
msgid "Booking Form"
msgstr "Formulario de Reserva\n"

#: ../includes/WP_Widget.BookingFormWidget.class.php:26
msgid "Display a form to accept bookings."
msgstr ""

#: ../includes/WP_Widget.BookingFormWidget.class.php:64
msgid "Title"
msgstr "Titulo"

#: ../lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.class.php:266
msgid "Add new scheduling rule"
msgstr "Agregar nueva regla de reserva"

#: ../lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.class.php:293 ..
#: lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.class.php:306
msgctxt "Format of a scheduling rule"
msgid "Weekly"
msgstr "Semanal"

#: ../lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.class.php:295 ..
#: lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.class.php:314
msgctxt "Format of a scheduling rule"
msgid "Monthly"
msgstr "Mensual"

#: ../lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.class.php:297 ..
#: lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.class.php:322
msgctxt "Format of a scheduling rule"
msgid "Date"
msgstr "Fecha"

#: ../lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.class.php:333
msgctxt "Label for selecting days of the week in a scheduling rule"
msgid "Days of the week"
msgstr "Dias"

#: ../lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.class.php:349
msgctxt "Label for selecting weeks of the month in a scheduling rule"
msgid "Weeks of the month"
msgstr "Semana del mes"

#: ../lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.class.php:380
msgctxt "Label to select time slot for a scheduling rule"
msgid "Time"
msgstr "Hora"

#: ../lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.class.php:385
msgctxt "Label to set a scheduling rule to last all day"
msgid "All day"
msgstr "Todo el dia"

#: ../lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.class.php:394
msgctxt "Label for the starting time of a scheduling rule"
msgid "Start"
msgstr "Inicio"

#: ../lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.class.php:402
msgctxt "Label for the ending time of a scheduling rule"
msgid "End"
msgstr "Fin"

#: ../lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.class.php:411
msgctxt "Prompt displayed when a scheduling rule is set without any time restrictions."
msgid ""
"All day long. Want to <a href=\"#\" data-format=\"time-slot\">set a time "
"slot</a>?"
msgstr ""

#: ../lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.class.php:430 ..
#: lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.class.php:433
msgid "Open and close this rule"
msgstr ""

#: ../lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.class.php:436
msgid "Delete rule"
msgstr "Eliminar regla"

#: ../lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.class.php:439
msgid "Delete scheduling rule"
msgstr ""

#: ../lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.class.php:523
msgctxt ""
"Brief default description of a scheduling rule when no weekdays or weeks are "
"included in the rule."
msgid "Never"
msgstr "Nunca"

#: ../lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.class.php:524
msgctxt ""
"Brief default description of a scheduling rule when all the weekdays/weeks "
"are included in the rule."
msgid "Every day"
msgstr "Cada dia"

#: ../lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.class.php:525
msgctxt ""
"Brief default description of a scheduling rule when some weekdays are "
"included on only some weeks of the month. The {days} and {weeks} bits should "
"be left alone and will be replaced by a comma-separated list of days (the "
"first one) and weeks (the second one) in the following format: M, T, W on "
"the first, second week of the month"
msgid "{days} on the {weeks} week of the month"
msgstr "{days} en la {weeks} semana del mes."

#: ../lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.class.php:526
msgctxt ""
"Brief description of a scheduling rule when some weeks of the month are "
"included but all or no weekdays are selected.  {weeks}  should be left alone "
"and will be replaced by a comma-separated list of weeks (the second one) in "
"the following format: First, second week of the month"
msgid "{weeks} week of the month"
msgstr ""

#: ../lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.class.php:527
msgctxt "Brief default description of a scheduling rule when no times are set"
msgid "All day"
msgstr ""

#: ../lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.class.php:528
msgctxt ""
"Brief default description of a scheduling rule when an end time is set but "
"no start time. If the end time is 6pm, it will read: Ends at 6pm."
msgid "Ends at"
msgstr ""

#: ../lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.class.php:529
msgctxt ""
"Brief default description of a scheduling rule when a start time is set but "
"no end time. If the start time is 6pm, it will read: Starts at 6pm."
msgid "Starts at"
msgstr ""

#: ../lib/simple-admin-pages/classes/AdminPageSetting.Scheduler.class.php:530
msgctxt "Default separator between times of a scheduling rule."
msgid "&mdash;"
msgstr ""

#: ../includes/Booking.class.php:175
msgid "Please enter the time you would like to book."
msgstr "Por favor la hora que desea reservar"

#: ../includes/Booking.class.php:185
msgid ""
"The time you entered is not valid. Please select from one of the times "
"provided."
msgstr ""
"La hora ingresada no es valida. \n"
"Por favor seleccione una de las opciones."

#: ../includes/Booking.class.php:204
#, php-format
msgid "Sorry, bookings can not be made more than %s days in advance."
msgstr ""
"Disculpe. Las reservas no pueden ser con mas \n"
" %s dias de \n"
"anticipación\n"
"."

#: ../includes/Booking.class.php:215
msgid "Sorry, bookings can not be made in the past."
msgstr "Las reservas no se pueden hacer en fechas pasadas."

#: ../includes/Booking.class.php:223
#, php-format
msgid "Sorry, bookings must be made more than %s days in advance."
msgstr "Disculpe, las reservas deben ser hechas con mas de %s dias de anticipación. "

#: ../includes/Booking.class.php:225
#, php-format
msgid "Sorry, bookings must be made more than %s hours in advance."
msgstr "Disculpe, las reservas deben ser hechas con mas de %s horas de anticipación. "

#: ../includes/Booking.class.php:227
#, php-format
msgid "Sorry, bookings must be made more than %s mings in advance."
msgstr ""
"Disculpe, las reservas deben ser hechas con mas de %s minutos de "
"anticipación. "

#: ../includes/Booking.class.php:266
msgid "Sorry, no bookings are being accepted then."
msgstr ""
"Disculpe, no se \n"
"están\n"
" realizando reservas."

#: ../includes/Booking.class.php:318
msgid "Sorry, no bookings are being accepted on that date."
msgstr "Disculpe, no se  están realizando reservas para esa fecha."

#: ../includes/Booking.class.php:324
msgid "Sorry, no bookings are being accepted at that time."
msgstr "Disculpe, no se  están realizando reservas para esa hora."

#: ../includes/Booking.class.php:346
msgid "Please enter a name for this booking."
msgstr "Por favor ingrese un nombre para esta reserva."

#: ../includes/Booking.class.php:356
msgid "Please let us know how many people will be in your party."
msgstr "Por favor dejenos saber para cuantos asistentes es su reserva."

#: ../includes/Booking.class.php:370
#, php-format
msgid "We only accept bookings for parties of up to %d people."
msgstr ""
"Solo aceptamos reservas para grupos de \n"
"%d personas."

#: ../includes/Booking.class.php:381
msgid "Please enter an email address so we can confirm your booking."
msgstr "Por favor ingrese una dirección de email para poder confirmar su reserva."

#: ../includes/CustomPostTypes.class.php:38 ../includes/CustomPostTypes.class.php:
#: 40 ../includes/CustomPostTypes.class.php:41 ../includes/WP_List_Table.
#: BookingsTable.class.php:76
msgid "Bookings"
msgstr "Reservas"

#: ../includes/CustomPostTypes.class.php:39 ../includes/WP_List_Table.
#: BookingsTable.class.php:75
msgid "Booking"
msgstr "Reserva"

#: ../includes/CustomPostTypes.class.php:42
msgid "Add New"
msgstr "Agregar nueva"

#: ../includes/CustomPostTypes.class.php:43
msgid "Add New Booking"
msgstr "Agregar nueva reserva"

#: ../includes/CustomPostTypes.class.php:44
msgid "Edit Booking"
msgstr "Editar Reserva"

#: ../includes/CustomPostTypes.class.php:45
msgid "New Booking"
msgstr ""
"Nueva \n"
"Reserva"

#: ../includes/CustomPostTypes.class.php:46
msgid "View Booking"
msgstr ""
"Ver \n"
"Reserva"

#: ../includes/CustomPostTypes.class.php:47
msgid "Search Bookings"
msgstr ""
"Buscar \n"
"Reserva"

#: ../includes/CustomPostTypes.class.php:48
msgid "No bookings found"
msgstr "No se encontraron reservas"

#: ../includes/CustomPostTypes.class.php:49
msgid "No bookings found in trash"
msgstr "No se encontraron reservas en papelera"

#: ../includes/CustomPostTypes.class.php:50
msgid "All Bookings"
msgstr ""
"Todas las \n"
"Reservas"

#: ../includes/CustomPostTypes.class.php:80
msgctxt "Booking status when it is pending review"
msgid "Pending"
msgstr "Pendiente"

#: ../includes/CustomPostTypes.class.php:86
msgctxt "Booking status for a confirmed booking"
msgid "Confirmed"
msgstr "Confirmado"

#: ../includes/CustomPostTypes.class.php:93
#, php-format
msgid "Confirmed <span class=\"count\">(%s)</span>"
msgid_plural "Confirmed <span class=\"count\">(%s)</span>"
msgstr[0] "Confirmado"
msgstr[1] "Confirmadas"

#: ../includes/CustomPostTypes.class.php:97
msgctxt "Booking status for a closed booking"
msgid "Closed"
msgstr "Cerrado"

#: ../includes/CustomPostTypes.class.php:104
#, php-format
msgid "Closed <span class=\"count\">(%s)</span>"
msgid_plural "Closed <span class=\"count\">(%s)</span>"
msgstr[0] "Cerrado"
msgstr[1] "Cerrados"

#: ../includes/Notification.class.php:87
msgid "View pending bookings"
msgstr "Ver reservas pendientes"

#: ../includes/Notification.class.php:88
msgid "Confirm this booking"
msgstr "Confirmar esta reserva"

#: ../includes/Notification.class.php:89
msgid "Reject this booking"
msgstr "Rechazar esta reserva"

#: ../includes/Settings.class.php:83
msgid ""
"Thanks, your booking request is waiting to be confirmed. Updates will be "
"sent to the email address you provided."
msgstr ""
"Gracias, su reserva esta esperando confirmación. Las novedades le serán "
"enviadas por email."

#: ../includes/Settings.class.php:96
msgctxt "Default email subject for admin notifications of new bookings"
msgid "New Booking Request"
msgstr "Nueva solicitud de reserva"

#: ../includes/Settings.class.php:97
msgctxt ""
"Default email sent to the admin when a new booking request is made. The tags "
"in {brackets} will be replaced by the appropriate content and should be left "
"in place. HTML is allowed, but be aware that many email clients do not "
"handle HTML very well."
msgid ""
"A new booking request has been made at {site_name}:\n"
"\n"
"{user_name}\n"
"{party} people\n"
"{date}\n"
"\n"
"{bookings_link}\n"
"{confirm_link}\n"
"{close_link}\n"
"\n"
"&nbsp;\n"
"\n"
"<em>This message was sent by {site_link} on {current_time}.</em>"
msgstr ""
"Una nueva reserva {site_name}:\n"
"\n"
"{user_name}\n"
"{party} personas\n"
"{date}\n"
"\n"
"{bookings_link}\n"
"{confirm_link}\n"
"{close_link}\n"
"\n"
"&nbsp;\n"
"\n"
"<em>Mensaje enviado {site_link} el {current_time}.</em>"

#: ../includes/Settings.class.php:115
#, php-format
msgctxt ""
"Default email subject sent to user when they request a booking. %s will be "
"replaced by the website name"
msgid "Your booking at %s is pending"
msgstr "Su reserva %s esta pendiente"

#: ../includes/Settings.class.php:116
msgctxt ""
"Default email sent to users when they make a new booking request. The tags "
"in {brackets} will be replaced by the appropriate content and should be left "
"in place. HTML is allowed, but be aware that many email clients do not "
"handle HTML very well."
msgid ""
"Thanks {user_name},\n"
"\n"
"Your booking request is <strong>waiting to be confirmed</strong>.\n"
"\n"
"Give us a few moments to make sure that we've got space for you. You will "
"receive another email from us soon. If this request was made outside of our "
"normal working hours, we may not be able to confirm it until we're open "
"again.\n"
"\n"
"<strong>Your request details:</strong>\n"
"{user_name}\n"
"{party} people\n"
"{date}\n"
"\n"
"&nbsp;\n"
"\n"
"<em>This message was sent by {site_link} on {current_time}.</em>"
msgstr ""
"Gracias {user_name},\n"
"\n"
"Su reserva <strong>espera confirmacion</strong>.\n"
"\n"
"Permitanos procesar su reserva. \n"
"Recibirá\n"
" un mensaje pronto. Si este proceso es fuera de nuestras horas laborales, "
"sera respondido en la \n"
"próxima\n"
" jornada.\n"
"\n"
"<strong>Detalles de su reserva:</strong>\n"
"{user_name}\n"
"{party} personas\n"
"{date}\n"
"\n"
"&nbsp;\n"
"\n"
"<em>Mensaje enviado {site_link} el {current_time}.</em>"

#: ../restaurant-reservations.php:127
msgid "Booking Manager"
msgstr "Administrador de Reservas"

#: ../restaurant-reservations.php:155
msgctxt "Title of admin page that lists bookings"
msgid "Bookings"
msgstr "Reservas"

#: ../restaurant-reservations.php:156
msgctxt "Title of bookings admin menu item"
msgid "Bookings"
msgstr "Reservas"

#: ../restaurant-reservations.php:178
msgid "Restaurant Bookings"
msgstr "Reservas de Restaurante"

#: ../restaurant-reservations.php:279
msgid "View the help documentation for Restaurant Reservations"
msgstr "Ver documentacion"

#: ../restaurant-reservations.php:279
msgid "Help"
msgstr "Ayuda"

#: ../includes/Booking.class.php:154
msgid "Please enter the date you would like to book."
msgstr "Por favor ingrese la fecha que desea reservar"

#: ../includes/Booking.class.php:164
msgid ""
"The date you entered is not valid. Please select from one of the dates in "
"the calendar."
msgstr "La fecha ingresada no es valida, por favor seleccione una del calendario."
