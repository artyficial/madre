<?php 
/* 
Template Name: Full Width Portada
*/
?>
<?php get_header();?>

<?php
	$location = icore_get_location();   
	$meta = icore_get_multimeta(array('Subheader')); 
?>

<div id="entry-full" class="fullWidthPortada">
    
    <div id="page-top">	 
    	
    	<h1 class="title"><?php  the_title();  ?></h1>
    	
    </div> 


    <div id="page-body">

		<section class="homeSlice" id ="homeBoard">
	
	        <h2 class="dia">Para hoy <?php 
	        	$dias = array("domingo","lunes","martes","miercoles","jueves","viernes","sábado");
				echo $dias[date('w')]." ".date('d') ; ?> tenemos:
			</h2>
		
	        <div class="board"> 
				<?php query_posts('cat=7');
					while ( have_posts() ) : the_post();
						echo '<h2>'; the_title(); echo '</h2>';
						the_content();
					endwhile; ?>
	        </div>
		</section>

		<section class="homeSlice" id="call2actSlice">
			
			<div class="typolografia">
			<a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						<?php if ( isset( $options['logo'] ) && '' != $options['logo'] ) : ?>
				    		<img id="logo" src="<?php echo esc_url( $options['logo'] ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
				    	<?php else : ?>
				    		<?php bloginfo( 'name' ); ?>
				    	<?php endif; ?>
				    </a>

				<p><?php bloginfo('description');?> </p>
			</div>

			<div class="call2act">
				<div id="quote">
				    <p class="quote">Nuestras mesas tienen alta demanda</p>
				    <p class="quote-second">Reserve con anticipacion y elija la ubicacion preferida</p> 
				    <a href="/reservas/" class="action-button"><span>Reserva online</span></a>
				</div>
				<p class="llamanos">Llamanos +57 313 840 8116</p>
			</div>
		</section>

		<section class="homeSlice" id="artistasSlice">
			<h2 class="h2separa">Artistas</h2>
			<div>
				<?php query_posts('category_name=artistas');
					while ( have_posts() ) : the_post();
						echo '<div class="blogPost"> '; 
						echo '<a href="'; the_permalink(); echo '">';
						echo '<h2>'; the_title(); echo '</h2>';
						the_post_thumbnail();
						the_excerpt();
						echo '</a></div>';
					endwhile;
				?>
			</div>
		</section>
		
		<section class="homeSlice" id="eventosSlice">
			<h2 class="h2separa">Eventos</h2>
			<div>
				<?php query_posts('category_name=eventos'); 
				while ( have_posts() ) : the_post();
						echo '<div class="blogPost"> '; 
						echo '<a href="'; the_permalink(); echo '">';
						echo '<h2>'; the_title(); echo '</h2>';
						the_post_thumbnail();
						the_excerpt();
						echo '</a></div>';
					endwhile;?>
			</div>
		</section>
   
    </div> 

</div> 
<!-- #entry-full  -->
<?php get_footer(); ?>