<?php global $options; ?>

		</div> <!-- #main-content  --> 
	</div>  <!-- #wrap-inside  -->
    <?php if ( is_active_sidebar( 'footer-sidebar' ) ) : ?>
		<footer id="footer">
			<div id="footer-inside">
				<div id="footer-widgets">	
					<?php dynamic_sidebar( 'footer-sidebar' ); ?>
			    </div> <!-- #footer-widgets --> 
			</div> <!-- #footer-inside --> 
			<div class="clear"></div>
		</footer> <!--  #footer  -->
	<?php endif; ?>

	
	<div id="copyright">             
    	<div id="copy-text"> Todos los derechos reservados &copy 2014  - Powered by ||Artyficial|</div>

	</div> <!-- #copyright  -->

</div> <!-- #wrapper  -->
<?php wp_footer(); ?>
 
</body>
</html>